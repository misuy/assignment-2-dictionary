%include "lib.inc"
%include "colon_params.inc"

section .text

; input:
; rdi -- key pointer
; rsi -- first node pointer
; output:
; rax -- pointer to wanted node or 0 if not found
global find_word
find_word:
    .loop:
    cmp rsi, first_node_pointer
    je .not_found
    push rdi
    push rsi
    add rsi, shift_to_key
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 0
    je .iterate
    mov rax, rsi
    jmp .end
    .iterate:
    mov rsi, qword[rsi]
    jmp .loop

    .not_found:
    xor rax, rax

    .end:
    ret

; input:
; rdi -- node pointer
; output:
; rax -- value pointer
global get_value
get_value:
    push rdi
    add rdi, shift_to_key
    call string_length
    pop rdi
    add rax, rdi
    add rax, shift_to_key
    inc rax
    ret