%include "colon_params.inc"

%macro colon 2

%2: dq first_node_pointer ; pointer to the first element
db %1, 0 ; key
%define first_node_pointer %2

%endmacro