%include "colon.inc"

section .data

colon "first_element", first_element
db "first element value", 0

colon "second_element", second_element
db "second element value", 0

colon "unexpected fourth element", fourth_element
db ":33", 0

colon "itmo", itmo
db "IT's MOre than a university", 0