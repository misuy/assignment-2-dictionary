%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define max_key_size 255

section .bss
key_buffer: resb max_key_size

section .rodata
greeting_message: db "Hi! Please type your key here (length <= 255):", 0
bad_key_message: db "Your key is bad.", 0
success_message: db "Value by your key found:", 0
fail_message: db "Value by your key not found.", 0

section .text

global _start
_start:
    mov rdi, greeting_message
    call print_string
    call print_newline
    mov rdi, key_buffer
    mov rsi, max_key_size
    call read_string
    cmp rax, 0
    je .bad_key
    mov rdi, rax
    mov rsi, first_node_pointer
    call find_word
    cmp rax, 0
    je .not_found
    mov rdi, rax
    call get_value
    push rax
    mov rdi, success_message
    call print_string
    call print_newline
    pop rdi
    call print_string
    call print_newline
    xor rdi, rdi
    jmp .end
    

    .bad_key:
    mov rdi, bad_key_message
    call print_err_string
    call print_err_newline
    mov rdi, 1
    jmp .end

    .not_found:
    mov rdi, fail_message
    call print_err_string
    call print_err_newline
    mov rdi, 2
    jmp .end

    .end:
    call exit